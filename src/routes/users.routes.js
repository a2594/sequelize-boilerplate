const {Router} = require("express");
const {getAllUsers, getUserById, createUser, updateUser, deleteUser} = require("../controllers/users.controller");
const {conversationsFromUser} = require("../controllers/conversations.controller");
const {validateToken, restrictedMiddleware} = require("../middlewares/auth.middleware");

const router = Router();

//GET -> obtener todos los registros
router.get("/users", validateToken, getAllUsers);
//GET -> obtener un registro por id
router.get("/users/:id", validateToken, getUserById);
//POST -> Agregar un registro
router.post("/users", createUser);
//UPDATE -> Actualizar un registro
router.put("/users/:id", validateToken, restrictedMiddleware, updateUser);
//DELETE -> Borrar un registro
router.delete("/users/:id", validateToken, restrictedMiddleware, deleteUser);

//Completar la siguiente ruta
router.get("/users/:id/conversations", conversationsFromUser);

module.exports = router;